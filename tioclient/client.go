package tioclient

import (
	"errors"
	"os"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

func Getclient() (*twitter.Client, error) {
	var client *twitter.Client

	if os.Getenv("consumerKey") == "" {
		return nil, errors.New("No consumerKey")
	}
	if os.Getenv("consumerSecret") == "" {
		return nil, errors.New("No consumerSecret")
	}
	if os.Getenv("accessToken") == "" {
		return nil, errors.New("No accessToken")
	}
	if os.Getenv("accessSecret") == "" {
		return nil, errors.New("No accessSecret")
	}

	config := oauth1.NewConfig(os.Getenv("consumerKey"), os.Getenv("consumerSecret"))
	token := oauth1.NewToken(os.Getenv("accessToken"), os.Getenv("accessSecret"))
	// OAuth1 http.Client will automatically authorize Requests
	httpClient := config.Client(oauth1.NoContext, token)

	// Twitter Client
	client = twitter.NewClient(httpClient)
	return client, nil

}
