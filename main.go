package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/brendan-demo/tiobruno/tioclient"
	"gitlab.com/brendan-demo/tiobruno/tiowords"

	"github.com/dghubble/go-twitter/twitter"
)

func main() {
	client, err := tioclient.Getclient()

	if err != nil {
		log.Fatal((err))
	}

	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		//Update(status string, params *twitter.StatusUpdateParams)
		newParams := &twitter.StatusUpdateParams{
			InReplyToStatusID: tweet.ID,
		}
		phrase := tiowords.Getresponse()
		status := "@" + tweet.User.ScreenName + " " + phrase
		ourTweet, _, err := client.Statuses.Update(status, newParams)
		if err != nil {
			log.Println("Error sending tweet")
			log.Println(err)
		}

		log.Println("@", tweet.User.ScreenName, "said ", tweet.Text)
		log.Println("We said: '", ourTweet.Text, "'")
	}

	log.Print("Stream starting")

	// FILTER
	filterParams := &twitter.StreamFilterParams{
		Track:         []string{"@TioBrunoBot"},
		StallWarnings: twitter.Bool(true),
	}
	stream, err := client.Streams.Filter(filterParams)
	if err != nil {
		log.Fatal(err)
	}

	// Receive messages until stopped or stream quits
	go demux.HandleChan(stream.Messages)

	// Wait for SIGINT and SIGTERM (HIT CTRL-C)
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	log.Println(<-ch)

	log.Println("Stopping Stream")
	stream.Stop()
}
