module gitlab.com/brendan-demo/tiobruno

// +heroku goVersion go1.17

go 1.17

require golang.org/x/example v0.0.0-20210811190340-787a929d5a0d

require (
	github.com/cenkalti/backoff/v4 v4.1.2 // indirect
	github.com/dghubble/go-twitter v0.0.0-20211115160449-93a8679adecb
	github.com/dghubble/oauth1 v0.7.1
	github.com/dghubble/sling v1.4.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
)
